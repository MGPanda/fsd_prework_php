<?php
    function diaSemana($numero) {
        switch($numero) {
            case 1:
                echo "Hoy es lunes.\n";
            break;

            case 2:
                echo "Hoy es martes.\n";
            break;

            case 3:
                echo "Hoy es miércoles.\n";
            break;

            case 4:
                echo "Hoy es jueves.\n";
            break;

            case 5:
                echo "Hoy es viernes.\n";
            break;

            case 6:
                echo "Hoy es sábado.\n";
            break;

            case 7:
                echo "Hoy es domingo.\n";
            break;

            default:
            echo "Tienes que introducir un número del 1 al 7.\n";
        break;
        }
    }

    diaSemana(1);
    diaSemana(5);
    diaSemana(10);
?>
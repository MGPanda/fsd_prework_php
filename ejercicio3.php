<?php
    $quijote = fopen("el_quijote.txt", "r");
    $contador = 0;
    while (!feof($quijote)) {
        $linea = strtoupper(fgets($quijote));
        $lineaArray = explode(" ",$linea);
        if (in_array("MOLINO",$lineaArray)) {
            $contador++;
        }
    }
    fclose($quijote);
    echo "La palabra \"molino\" aparece $contador veces en El Quijote.";
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Ejercicios PHP</title>
        <meta charset="UTF-8"/>
    </head>
    <body>
        <h1>Ejercicio 2</h1>
        <form action="ejercicio2.php" method="GET">
            <input name="stringRecibido" type="text"/>
            <button name="submit2" type="submit">Comprobar</button>
        </form>

        <h1>Ejercicio 6</h1>
        <form action="ejercicio6.php" method="POST">
            <input name="correo" type="text" placeholder="Correo electrónico"/>
            <input name="password" type="password" placeholder="Contraseña"/>
            <input name="passwordCheck" type="password" placeholder="Confirmar contraseña"/>
            <button name="submit6" type="submit">Registrarse</button>
        </form>
    </body>
</html>
<?php
    if (isset($_GET['submit2'])) {
        $stringRecibido = $_GET['stringRecibido'];
    
        function contieneVocales($mensaje) {
            $contenido = str_split(strtoupper($mensaje));
            if (in_array('A',$contenido) && in_array('E',$contenido) && in_array('I',$contenido) && in_array('O',$contenido) && in_array('U',$contenido)) {
                return true;
            } else {
                return false;
            }
        }

        function imprimirRespuesta($confirmacion) {
            if ($confirmacion) {
                echo "LA PALABRA CONTIENE LAS 5 VOCALES\n";
            } else {
                echo "NO CONTIENE TODAS LAS VOCALES\n";
            }
        }

        imprimirRespuesta(contieneVocales($stringRecibido));
    } 
?>
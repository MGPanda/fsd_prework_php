<?php
    function reemplazar($ruta, $rutaNueva, $palabra, $palabraNueva) {
        $archivo = fopen($ruta, "r");
        if ($archivo != null) {
            $archivoNuevo = fopen($rutaNueva, "w");
            while (!feof($archivo)) {
                fwrite($archivoNuevo, str_replace($palabra, $palabraNueva, fgets($archivo)));
            }
        }
    }

    reemplazar("el_quijote.txt","quijote_modificado.txt","Sancho","Morty")
?>